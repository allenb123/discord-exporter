package main

import (
	"encoding/json"
	"flag"
	"fmt"
	discord "github.com/bwmarrin/discordgo"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

var token string
var usersStr string
var outFile string
var indent bool
var maxMessages uint

func init() {
	flag.StringVar(&token, "t", "", "the user's discord token")
	flag.StringVar(&usersStr, "u", "", "the recipients, separated by ,")
	flag.StringVar(&outFile, "o", "out.json", "the output file")
	flag.UintVar(&maxMessages, "n", 50, "the number of messages to retrieve per DM")
	flag.BoolVar(&indent, "i", false, "indent output file")
}

func main() {
	flag.Parse()
	if token == "" {
		panic("token required")
	}

	s, err := discord.New(token)
	if err != nil {
		panic(err)
	}

	log.Println("starting...")

	s.AddHandler(func(s *discord.Session, r *discord.Ready) {
		out := make(map[string][]string)

		users := strings.Split(usersStr, ",")

		log.Println("You are: " + r.User.Username + "#" + r.User.Discriminator)
		for _, channel := range r.PrivateChannels {
			if channel.Type == discord.ChannelTypeDM && len(channel.Recipients) != 0 {
				recipient := channel.Recipients[0]
				recipientName := recipient.Username + "#" + recipient.Discriminator

				found := false
				for _, user := range users {
					if user == recipientName {
						found = true
						break
					}
				}

				if !found {
					continue
				}

				lastId := ""
				msgCount := uint(0)
			outer:
				for {
					msgs, err := s.ChannelMessages(channel.ID, 100, lastId, "", "")
					if err != nil {
						log.Println("error: ", err)
						return
					}
					if len(msgs) == 0 {
						break
					}
					lastId = msgs[len(msgs)-1].ID

					for _, msg := range msgs {
						if msg.Author.ID == recipient.ID {
							out[recipientName] = append(out[recipientName], msg.Content)
							msgCount++
						}

						if msgCount >= maxMessages {
							break outer
						}
					}
				}
				log.Println(recipientName + ": " + fmt.Sprint(msgCount))
			}
		}

		var body []byte
		if indent {
			body, _ = json.MarshalIndent(out, "", "\t")
		} else {
			body, _ = json.Marshal(out)
		}
		file, err := os.Create(outFile)
		if err != nil {
			log.Println(err)
			return
		}
		file.Write(body)

		s.Close()
		os.Exit(0)
	})

	err = s.Open()
	if err != nil {
		panic(err)
	}

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	s.Close()
}
